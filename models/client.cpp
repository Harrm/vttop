#include <TreeFrogModel>
#include "client.h"
#include "sqlobjects/clientobject.h"

Client::Client()
    : TAbstractModel(), d(new ClientObject())
{
    d->id = 0;
}

Client::Client(const Client &other)
    : TAbstractModel(), d(other.d)
{ }

Client::Client(const ClientObject &object)
    : TAbstractModel(), d(new ClientObject(object))
{ }

Client::~Client()
{
    // If the reference count becomes 0,
    // the shared data object 'ClientObject' is deleted.
}

int Client::id() const
{
    return d->id;
}

void Client::setId(int id)
{
    d->id = id;
}

QString Client::name() const
{
    return d->name;
}

void Client::setName(const QString &name)
{
    d->name = name;
}

Client &Client::operator=(const Client &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Client Client::create(int id, const QString &name)
{
    ClientObject obj;
    obj.id = id;
    obj.name = name;
    if (!obj.create()) {
        return Client();
    }
    return Client(obj);
}

Client Client::create(const QVariantMap &values)
{
    Client model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Client Client::get(int id)
{
    TSqlORMapper<ClientObject> mapper;
    return Client(mapper.findByPrimaryKey(id));
}

int Client::count()
{
    TSqlORMapper<ClientObject> mapper;
    return mapper.findCount();
}

QList<Client> Client::getAll()
{
    return tfGetModelListByCriteria<Client, ClientObject>(TCriteria());
}

QJsonArray Client::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<ClientObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<ClientObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Client(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Client::modelData()
{
    return d.data();
}

const TModelObject *Client::modelData() const
{
    return d.data();
}

QDataStream &operator<<(QDataStream &ds, const Client &model)
{
    auto varmap = model.toVariantMap();
    ds << varmap;
    return ds;
}

QDataStream &operator>>(QDataStream &ds, Client &model)
{
    QVariantMap varmap;
    ds >> varmap;
    model.setProperties(varmap);
    return ds;
}

// Don't remove below this line
T_REGISTER_STREAM_OPERATORS(Client)
