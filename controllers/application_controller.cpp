#include "application_controller.hpp"

ApplicationController::ApplicationController() : TActionController{} {}

void ApplicationController::staticInitialize() {}

void ApplicationController::staticRelease() {}

bool ApplicationController::preFilter() { return true; }

// Don't remove below this line
T_DEFINE_CONTROLLER(ApplicationController)
