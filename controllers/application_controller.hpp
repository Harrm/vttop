#ifndef APPLICATIONCONTROLLER_H
#define APPLICATIONCONTROLLER_H

#include "applicationhelper.h"
#include <TActionController>

class T_CONTROLLER_EXPORT ApplicationController : public TActionController {

  Q_OBJECT

public:
  ApplicationController();
  ~ApplicationController() override = default;

public slots:
  void staticInitialize();
  void staticRelease();

protected:
  bool preFilter() override;
};

#endif // APPLICATIONCONTROLLER_H
