//
// Created by harrm on 27.07.2019.
//

#ifndef APPNAME_HELLO_CONTROLLER_HPP
#define APPNAME_HELLO_CONTROLLER_HPP

#include "application_controller.hpp"

class T_CONTROLLER_EXPORT HelloController: public ApplicationController {

  Q_OBJECT

public:
  ~HelloController() override = default;

public slots:
  void index();
  void show();
};

#endif // APPNAME_HELLO_CONTROLLER_HPP
