//
// Created by harrm on 27.07.2019.
//

#include "hello_controller.hpp"

void HelloController::index() {
  auto foo = httpRequest().queryItemValue("foo");
  texport(foo);
  render();
}

void HelloController::show() {
  auto bar = httpRequest().queryItemValue("bar");
  texport(bar);
  render();
}

// Don't remove below this line
T_DEFINE_CONTROLLER(HelloController)
